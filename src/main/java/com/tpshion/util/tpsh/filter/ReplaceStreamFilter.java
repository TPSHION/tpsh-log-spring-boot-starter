package com.tpshion.util.tpsh.filter;

import com.tpshion.util.tpsh.support.RequestWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * 替换HttpServletRequest
 */
public class ReplaceStreamFilter implements Filter {

    private static final Logger log = LoggerFactory.getLogger(ReplaceStreamFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.info("ReplaceStreamFilter init...");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        // 替换HttpServletRequest
        filterChain.doFilter(new RequestWrapper((HttpServletRequest) request), response);
    }

    @Override
    public void destroy() {
        log.info("ReplaceStreamFilter destroy...");
    }
}
