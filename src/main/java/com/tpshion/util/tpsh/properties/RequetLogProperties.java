package com.tpshion.util.tpsh.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 请求日志配置类
 */
@ConfigurationProperties("tpsh.log")
public class RequetLogProperties {

    /**
     * 是否启用，默认启用
     */
    private boolean enabled = true;

    /**
     * 优先级，默认最高：1
     */
    private Integer order = 1;

    /**
     * filter名称
     */
    private String filterName = "replaceStreamFilter";

    /**
     * filter拦截路径
     */
    private String path = "/*";

    /**
     * 不拦截路径，多个路径","英文逗号隔开
     */
    private String exclude = "";

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getFilterName() {
        return filterName;
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getExclude() {
        return exclude;
    }

    public void setExclude(String exclude) {
        this.exclude = exclude;
    }

    @Override
    public String toString() {
        return "RequetLogProperties{" +
                "enabled=" + enabled +
                ", order=" + order +
                ", filterName='" + filterName + '\'' +
                ", path='" + path + '\'' +
                ", exclude='" + exclude + '\'' +
                '}';
    }
}
