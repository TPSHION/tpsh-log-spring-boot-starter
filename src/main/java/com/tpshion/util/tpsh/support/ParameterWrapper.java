package com.tpshion.util.tpsh.support;

/**
 * 方法参数包装类
 */
public class ParameterWrapper {

    /**
     * 参数类型
     */
    private String type;

    /**
     * 参数名称
     */
    private String name;

    public ParameterWrapper() {
    }

    public ParameterWrapper(String type, String name) {
        this.type = type;
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
